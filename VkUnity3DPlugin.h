//
//  VkDelegate.h
//  Vk native implementation for Unity plugin
//
//  Created by Vlad Chartanovich on 18.09.15.
//  vmchar@outlook.com
//  Copyright (c) 2015 Vlad Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk/VKSdk.h>
#import "AppDelegateListener.h"

@interface VkDelegate : NSObject <VKSdkDelegate, AppDelegateListener>
{
    //Stores app id inside class
    @private NSString *applicationId;
    //Stores permissions
    @private NSArray *SCOPE;
    
}

#pragma mark - Public Methods

//Creates instance of VkDelegate and initializes
+ (VkDelegate*)initSDKWithAppID: (NSString*) appId;
+ (VkDelegate*) sharedInstance;

#pragma mark - Private Methods

//Constructor
- (id) initWithAppId: (NSString *) appId;

//Initialize VK SDK with given app id
- (void)startSDK;

//Set permissions for SDK
- (NSArray*)setScope;

//Try to login
- (void)login;

//Logout
- (void)logout;

//Status of login
- (BOOL)isLogined;

//Get a pair of token + ";" + userId
- (NSString*) getAccessToken;

//Call api method
- (void) callAPIWithMethod: (NSString *)method parameters: (NSDictionary *)params;


#pragma mark - Events

- (void) onLoginFinished: (VKAccessToken*) accesstoken;
- (void) onLoginFailed;
//Request failed callback
- (void) onApiError: (NSError *) error;


@end
