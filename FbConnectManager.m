//
//  FbConnectManager.m
//
//  Created by Vlad on 20.02.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import "FbConnectManager.h"
#import <Foundation/Foundation.h>
#import <Security/Security.h>
#import "SSKeychain.h"
#import "SingletonArc.h"
#import "RReachability.h"
#import "NSString+Extension.h"
#import "NSData+Extension.h"
#import <Foundation/NSHTTPCookieStorage.h>
#import "ResourceManager.h"
#import "BDManager.h"
#import "Global.h"
#import "AppViewController.h"
#import "SLQTSORAppDelegate.h"
#import "GameController.h"
#import "AbstractMapSelector.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation FbConnectManager

@synthesize UUID;
@synthesize transactionId;
@synthesize fbId;
@synthesize salt;
@synthesize clear_fb_id, clear_uid, is_main, logined, loginedNow;

#pragma mark Helper section

SYNTHESIZE_SINGLETON_FOR_ARC_CLASS(FbConnectManager);

dispatch_queue_t fbConnectRequests;
dispatch_queue_t fbPictures;


- (id) init
{
    if (self = [super init])
    {
        UUID = [self getUUID];
        fbConnectRequests = dispatch_queue_create("com.clonefish.bubblechronicles.fbConnectQueue", DISPATCH_QUEUE_SERIAL);
        fbPictures = dispatch_queue_create("com.clonefish.bubblechronicles.fbPictures", NULL);
        requests = [[NSMutableArray alloc] init];
        fbId = [[NSUserDefaults standardUserDefaults] integerForKey:@"fbId"];
        serverAddress = @"http://bubbles-ios.clonefish.com/";
        loginedNow = 0;

        salt = @"hqdOsrxGY6yrD9nBH_V7129AsW3OmUsK_FCoxi6C5v7ET0eFx3";

    }
    return self;
}

- (NSString *) createUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string ;
}

#pragma mark FIX sskeychain issue
- (NSString *)getUUID
{
    NSString *retrievedUUID = [SSKeychain passwordForService:@"com.renatus.bubblechronicles" account:@"user"];
    NSLog(@"UUID : %@", retrievedUUID);
    if (retrievedUUID.length == 0 ) {
        NSLog(@"UUID doesn't exist -> need to generate");
        retrievedUUID = [self createUUID];
        NSLog(@"Generated UUID : %@", retrievedUUID);
        [SSKeychain setPassword:retrievedUUID forService:@"com.renatus.bubblechronicles" account:@"user"];
    }
    return retrievedUUID;
}

- (BOOL) checkConnection
{
    RReachability *reach = [RReachability reachabilityWithHostName:@"bubblespredev-ios.clonefish.com"];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus != NotReachable) {    return YES;   }
    else{
        NSLog(@"WARNING: No internet connection");
        return NO;
    }
}

- (void) showCookies
{
    NSHTTPCookieStorage *cs = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cs cookiesForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", serverAddress]]];
    NSLog(@"Cookies array : %@", cookies);
    for (NSHTTPCookie *cookie in cookies) {
        NSLog(@"Name : %@ Value : %@", cookie.name, cookie.value);
    }
}

- (void) determineSyncTypeWithFb:(NSString *) gotFbId
{
    NSLog(@"Determine Sync Type");
    NSLog(@"Got parameters: clear_uid = %d clear_fb_id = %d is_main = %d", clear_uid, clear_fb_id, is_main);
    NSString *currentFbId = [[NSUserDefaults standardUserDefaults] objectForKey:@"curUserId"];
    NSString *savedFbId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSavedFbId"];
    NSLog(@"Got Fb id: %@, saved fbId: %@", gotFbId, savedFbId);
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"Syncing"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //When login is complete check fb id with saved one
    bool syncedWithCurrentFbId = false;
    if ([currentFbId isEqualToString:savedFbId]) {  syncedWithCurrentFbId = true;   }
    [[NSUserDefaults standardUserDefaults] setObject:currentFbId forKey:@"userSavedFbId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"syncedWithCurrentFbId = %d", syncedWithCurrentFbId);
    
    SLQTSORAppDelegate *del = [[UIApplication sharedApplication] delegate];
    AppViewController *cv = del.rootViewController;
    [cv fbConnection];
    
    if (clear_fb_id == 1 && clear_uid == 1) {   [self firstSyncRequest];    }
    else{
            if (clear_uid == 0 && clear_fb_id == 0 && is_main == 1 && syncedWithCurrentFbId == 1){    [self syncRequest];     }
            else{   [self getFbContextRequest]; }
    }
}

- (void) reloadIfNeeded
{
    GameController *gc = [GameController sharedGameController];
    [gc reloadSomeScene];
}

- (void) holdSyncResult: (NSString *)data
{
    BDManager *database = [BDManager sharedBDManager];
    [database dropFbTable];
    [self unpackData:data];
    [database dValuesRemove];
    [database calculateMaxLifes];
    [self reloadIfNeeded];
}

- (NSString *) createDataPackage
{
    NSMutableDictionary *package = [[NSMutableDictionary alloc] init];
    //Generate Era information
    NSLog(@"%s", __FUNCTION__);
    ResourceManager *rm = [[ResourceManager alloc] init];
    [rm saveEraIds];
    BDManager *database = [BDManager sharedBDManager];
    int numberOfEras = [[NSUserDefaults standardUserDefaults] integerForKey:@"EraCountInJSON"];
    NSMutableArray *locationsUDs = [[NSMutableArray alloc] init];
    int lastOpenEraId = [database getIntValue:@"lastOpenedEraId"];
    for (int i = 0; i < numberOfEras; ++i) {
        NSMutableDictionary *iLocationData = [[NSMutableDictionary alloc] init];
        NSString *eraID = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EraId%d", i]]];
        [iLocationData setObject:eraID forKey:@"locationModelId"];
        NSString *lock;
        (i > lastOpenEraId) ? (lock = [NSString stringWithFormat:@"%d", 1]) : (lock = [NSString stringWithFormat:@"%d", 0]);
        [iLocationData setObject:lock forKey:@"lock"];
        [locationsUDs addObject:iLocationData];
    }
    
    [package setObject:locationsUDs forKey:@"locationUDs"];
    
    NSMutableArray *lvlInfo = [[NSMutableArray alloc] init];
    
    //Generate level information
    for (int i = 0; i < lastOpenEraId + 1; ++i) {
        int *levelInfo = [rm eraLevelInfo:i];
        for (int j = 0; j < levelInfo[0]; ++j) {
            NSMutableDictionary *lvlDict = [[NSMutableDictionary alloc] init];
            //level id
            NSString *lvlId = [NSString stringWithFormat:@"%d", levelInfo[j + 1]];
            [lvlDict setObject:lvlId forKey:@"lvlModelId"];
            int score = [database getIntValue:[NSString stringWithFormat:@"Level%dBestScore",levelInfo[j+1]]];
            NSString *scoreValue = [NSString stringWithFormat:@"%d", score];
            [lvlDict setObject:scoreValue forKey:@"score"];
            [lvlInfo addObject:lvlDict];
            if (score == 0) {
                break;
            }
        }
        free(levelInfo);
    }
    
    [package setObject:lvlInfo forKey:@"lvlUDs"];
    
    //Collection items
    NSMutableArray *itemInfo = [[NSMutableArray alloc] init];
    int itemId = 0;
    for (int i = 0; i < lastOpenEraId + 1; ++i) {
        //5 items in each collection
        for (int j = 0; j < 5; ++j) {
            NSMutableDictionary *itemD = [[NSMutableDictionary alloc] init];
            int amount = [database getIntValue:[NSString stringWithFormat:@"%dEra%dItem", i, j]];
            int damount = [database getIntValue:[NSString stringWithFormat:@"d%dEra%dItem", i, j]];
            itemId = (i*5 + j);
            NSLog(@"%d", itemId);
            [itemD setObject:[NSString stringWithFormat:@"%d", itemId] forKey:@"collectionItemModelId"];
            [itemD setObject:[NSString stringWithFormat:@"%d", amount] forKey:@"count"];
            [itemD setObject:[NSString stringWithFormat:@"%d", damount] forKey:@"dCount"];
            [itemInfo addObject:itemD];
        }
    }
    
    [package setObject:itemInfo forKey:@"itemUDs"];
    
    //Key information
    NSMutableArray *keysInfo = [[NSMutableArray alloc] init];
    for (int i = 0; i < numberOfEras; ++i) {
        NSMutableDictionary *iLocationKey = [[NSMutableDictionary alloc] init];
        NSString *eraID = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EraId%d", i]]];
        [iLocationKey setObject:eraID forKey:@"keyModelId"];
        int amount = [database getIntValue:[NSString stringWithFormat:@"%dEraKeys", i]];
        int damount = [database getIntValue:[NSString stringWithFormat:@"d%dEraKeys", i]];
        [iLocationKey setObject:[NSString stringWithFormat:@"%d", amount] forKey:@"count"];
        [iLocationKey setObject:[NSString stringWithFormat:@"%d", damount] forKey:@"dCount"];
        [keysInfo addObject:iLocationKey];
    }
    
    [package setObject:keysInfo forKey:@"keyUDs"];
    
    //Amulet information
    NSMutableArray *amuletInfo = [[NSMutableArray alloc] init];
    int *amulets = [rm getAmuletInfo];
    int own = [database getIntValue:@"Abundance"];
    NSString *ownStr;
    
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *abundance = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[0]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:abundance];
    
    own = [database getIntValue:@"Eternal_Life"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *eternal_life = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[1]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:eternal_life];
    
    own = [database getIntValue:@"Vitality"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *vitality = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[2]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:vitality];
    
    own = [database getIntValue:@"Reflexio"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *reflexio = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[3]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:reflexio];
    
    own = [database getIntValue:@"Oracle"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *oracle = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[4]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:oracle];
    
    own = [database getIntValue:@"Strategy"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *strategy = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[5]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:strategy];
    
    own = [database getIntValue:@"Tesla"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *tesla = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[6]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:tesla];
    
    own = [database getIntValue:@"Constancy"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *constancy = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[7]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:constancy];
    
    own = [database getIntValue:@"Defrostio"];
    (own == 0) ? (ownStr = [NSString stringWithFormat:@"%d", 0]) : (ownStr = [NSString stringWithFormat:@"%d", 1]);
    NSDictionary *defrostio = [NSDictionary dictionaryWithObjects:@[[NSString stringWithFormat:@"%d", amulets[8]], ownStr, @"0", @"0"] forKeys:@[@"skillModelId", @"toOwn", @"chargeCount", @"tested"]];
    [amuletInfo addObject:defrostio];
    
    [package setObject:amuletInfo forKey:@"amuletUDs"];
    
    free(amulets);
    
    //Boost info
    NSMutableArray *boostArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 5; ++i) {
        NSMutableDictionary *boost = [[NSMutableDictionary alloc] init];
        int amount = [database getIntValue:[NSString stringWithFormat:@"Boost%d", i]];
        int damount = [database getIntValue:[NSString stringWithFormat:@"dBoost%d", i]];
        
        NSString *owned;
        (amount > 0) ? (owned = [NSString stringWithFormat:@"%d", 1]) : (owned = [NSString stringWithFormat:@"%d", 0]);
        [boost setObject:[NSString stringWithFormat:@"10%d", i] forKey:@"skillModelId"];
        [boost setObject:owned forKey:@"toOwn"];
        [boost setObject:[NSString stringWithFormat:@"%d", amount] forKey:@"chargeCount"];
        [boost setObject:[NSString stringWithFormat:@"%d", damount] forKey:@"dchargeCount"];
        [boostArray addObject:boost];
    }
    
    [package setObject:boostArray forKey:@"boostUDs"];
    
    //User resources info
    int cash = [database getIntValue:@"ingameCash"];
    int dcash = [database getIntValue:@"dingameCash"];
    int diamonds = [database getIntValue:@"ingameDiamonds"];
    int ddiamonds = [database getIntValue:@"dingameDiamonds"];
    
    [package setObject:[NSString stringWithFormat:@"%d", cash] forKey:@"cash"];
    [package setObject:[NSString stringWithFormat:@"%d", diamonds] forKey:@"diamond"];
    [package setObject:[NSString stringWithFormat:@"%d", dcash] forKey:@"dCash"];
    [package setObject:[NSString stringWithFormat:@"%d", ddiamonds] forKey:@"dDiamond"];
    
//    NSLog(@"Package : %@", package);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:package options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//    NSString *jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF16StringEncoding];

    
    return jsonString;
}

//Parce the fb data after sync
- (void) unpackData:(NSString *)recievedDataPackage
{
    NSLog(@"%s", __FUNCTION__);
    NSString *decodedData = [self decodeParamJSON:recievedDataPackage];
    NSData *decoded = [decodedData dataUsingEncoding:NSUTF8StringEncoding];
    if (![decoded isEqualToData:Nil]) {
        
    
    
    NSDictionary *structure = [NSJSONSerialization JSONObjectWithData:decoded options:0 error:Nil];
    NSLog(@"Decoded dictionary : %@", structure);
    
    BDManager *database = [BDManager sharedBDManager];
    database.context = FB;
    
    //User money
    int cash = [[structure objectForKey:@"cash"] integerValue];
    int diamonds = [[structure objectForKey:@"diamond"] integerValue];
    
    [database setIntValue:@"ingameCash" value:cash];
    [database setIntValue:@"ingameDiamonds" value:diamonds];
    
    //Passed locations info
    NSArray *locations = [structure objectForKey:@"locationUDs"];

    int openedLocations = 0;
    for (NSDictionary *era in locations) {
        int lock = [[era objectForKey:@"lock"] integerValue];
        if (lock == 0) {
            ++openedLocations;
        }
    }
    NSLog(@"locations opened : %d", openedLocations);
    
    if (openedLocations > 0) {
        --openedLocations;
    }
    //TODO: change locations number if added some
    if (openedLocations > 6) {
        openedLocations = 6;
    }
    [database setIntValue:@"currentEraId" value:openedLocations];
    [database setIntValue:@"lastOpenedEraId" value:openedLocations];
    
    //Passed levels, stars for levels and stars for eras over all
    NSArray *levels = [structure objectForKey:@"lvlUDs"];

    ResourceManager *rm = [[ResourceManager alloc] init];
    NSDictionary *starsInfo = [rm starsPerLevel:(openedLocations + 1)];
        
    //Set the flag to open era by keys if last era level passed
    NSLog(@"Getting info for unlock era");
    int levelId = [rm returnEraLastLevelIdWithEraNumber: openedLocations];
    int scoreForTheLevel = [database getIntValue:[NSString stringWithFormat:@"Level%dBestScore", levelId]];
    if (scoreForTheLevel > 0) {
        //Set the flag
        [[NSUserDefaults standardUserDefaults] setInteger:12 forKey:@"allEraLevelsPassed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
        
    for (NSDictionary *level in levels) {
        int score = [[level objectForKey:@"score"] integerValue];
        //Save score for passed levels
        if (score > 0) {
            //Get id
            NSString *number = [level objectForKey:@"lvlModelId"];
            [database setIntValue:[NSString stringWithFormat:@"Level%@BestScore", number] value:score];
            //Calcutale level stars and recount location stars
            NSDictionary *levelInfo = [starsInfo objectForKey:number];
            NSString *locationNumber = [levelInfo objectForKey:@"location"];
            NSArray *starsForScore = [levelInfo objectForKey:@"score"];
            int gotStars = 0;
            int star1 = [starsForScore[0] integerValue];
            int star2 = [starsForScore[1] integerValue];
            int star3 = [starsForScore[2] integerValue];
            
            if (score >= star1 && score < star2) {
                gotStars = 1;
            }
            if (score >= star2 && score < star3) {
                gotStars = 2;
            }
            if (score >= star3) {
                gotStars = 3;
            }
            //Set stars for level and location
            int alreadyGotStars = [database getIntValue:[NSString stringWithFormat:@"%@Era%@lvlStars", locationNumber, number]];
            [database setIntValue:[NSString stringWithFormat:@"%@Era%@lvlStars", locationNumber, number] value:gotStars];
            int locationStarsOverAll = [database getIntValue:[NSString stringWithFormat:@"era%@Stars", locationNumber]];
            locationStarsOverAll += gotStars;
            locationStarsOverAll -= alreadyGotStars;
            [database setIntValue:[NSString stringWithFormat:@"era%@Stars", locationNumber] value:locationStarsOverAll];
        }
        else{
            //Save last passed level
            if (score == 0) {
                NSString *number = [level objectForKey:@"lvlModelId"];
                NSDictionary *levelHelper = [rm allLevelsInfo];
                int levelNumber = [[levelHelper objectForKey:number] integerValue];
                [database setIntValue:@"lastPassedLevelNumber" value:levelNumber];
            }
        }
    }
    
    //Keys parse
    NSArray *keys = [structure objectForKey:@"keyUDs"];
    NSDictionary *locNums = [[NSDictionary alloc] initWithObjects:@[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"] forKeys:@[@"1", @"2", @"3", @"15", @"16", @"17", @"18", @"19", @"20", @"21"]];
    for (NSDictionary *keyInfo in keys) {
        NSString *loc = [NSString stringWithFormat:@"%d", [[keyInfo objectForKey:@"keyModelId"] integerValue]];
        int amount = [[keyInfo objectForKey:@"count"] integerValue];
        NSString *locId = [locNums objectForKey:loc];
        [database setIntValue:[NSString stringWithFormat:@"%@EraKeys", locId] value:amount];
    }
    //Boosts Parse
    NSArray *boosts = [structure objectForKey:@"boostUDs"];
    for (NSDictionary *boost in boosts) {
        int boostId = [[boost objectForKey:@"skillModelId"] integerValue];
        if (boostId >= 100 && boostId <= 104) {
            int charges = [[boost objectForKey:@"chargeCount"] integerValue];
            [database setIntValue:[NSString stringWithFormat:@"Boost%d", boostId] value:charges];
        }
    }
    //Collcetion items parse
    NSArray *items = [structure objectForKey:@"itemUDs"];
    for (NSDictionary *itemInfo in items) {
        int itemId = [[itemInfo objectForKey:@"collectionItemModelId"] integerValue];
        int amount = [[itemInfo objectForKey:@"count"] integerValue];
        int locationNumber, itemNumber;
        locationNumber = itemId / 5;
        itemNumber = itemId % 5;
        [database setIntValue:[NSString stringWithFormat:@"%dEra%dItem", locationNumber, itemNumber] value:amount];
    }
    //Amulets information parse
    NSArray *amulets = [structure objectForKey:@"amuletUDs"];
    int *amuletsIds = [rm getAmuletInfo];
    NSDictionary *listOfAmulets = [[NSDictionary alloc] initWithObjects:@[@"Abundance", @"Eternal_Life", @"Vitality", @"Reflexio", @"Oracle", @"Strategy", @"Tesla", @"Constancy", @"Defrostio"] forKeys:@[[NSString stringWithFormat:@"%d", amuletsIds[0]], [NSString stringWithFormat:@"%d", amuletsIds[1]], [NSString stringWithFormat:@"%d", amuletsIds[2]], [NSString stringWithFormat:@"%d", amuletsIds[3]], [NSString stringWithFormat:@"%d", amuletsIds[4]], [NSString stringWithFormat:@"%d", amuletsIds[5]], [NSString stringWithFormat:@"%d", amuletsIds[6]], [NSString stringWithFormat:@"%d", amuletsIds[7]], [NSString stringWithFormat:@"%d", amuletsIds[8]]]];
    for (NSDictionary *amulet in amulets) {
        NSString *curAmuletId = [NSString stringWithFormat:@"%d", [[amulet objectForKey:@"skillModelId"] integerValue]];
        int owned = [[amulet objectForKey:@"toOwn"] integerValue];
        NSString *curAmuletName = [listOfAmulets objectForKey:curAmuletId];
        [database setIntValue:curAmuletName value:owned];
        
    }
    free(amuletsIds);
    }
    NSLog(@"unpack data finished");
}

#pragma mark FaceBook Connect Requests

- (void) initRequest
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            NSString *paramDefenition = [self encodeParam:UUID];
            NSString *params = [NSString stringWithFormat:@"device_id=%@", paramDefenition];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@init", serverAddress]];
            NSLog(@"URL = %@ params : %@", url, params);
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
//            request.HTTPBody = [params dataUsingEncoding:NSUTF16StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&responce
                                                         error:&error];
            
            NSString *resp = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Response : %@", resp);
            
            if ((![data isEqualToData:nil]) && (!error)) {
            //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {
        
                    NSLog(@"JSON : %@", responceJSON);
    
                    NSString *status = [responceJSON objectForKey:@"status"];
                    NSLog(@"status : %@", status);
    
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : init status is : %@", status);
                        NSString *trId = [responceJSON objectForKey:@"transaction_id"];
                        transactionId = [trId integerValue];
                        NSLog(@"transaction id : %ld", transactionId);
                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : init failed with status : %@ and the reason is : %@", status, reason);
                    }
                }
                else{   NSLog(@"WARNING : init NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : init NIL data, error : %@", error);  }
        }
    });
}

- (void) pingRequest
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@device/ping", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if ((responceJSON != nil) && (!jerror)) {
                    //Hadle the responce status
                    NSString *status = [responceJSON objectForKey:@"status"];
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : ping status is %@", status);
                        NSLog(@"ping status ok!");
                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : ping failed with status : %@ and the reason is %@", status, reason);
                    }
                }
                else{   NSLog(@"WARNING : ping NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : ping NIL data, error : %@", error);  }
        }
    });
}

//@params : device_id, fb_id
- (void) loginRequest: (NSString *) fbIDnum
{
//    fbIDnum = [[NSUserDefaults standardUserDefaults] objectForKey:@"curUserId"];
    NSLog(@"%s", __FUNCTION__);
    if (loginedNow == 0) {
        NSLog(@"Not logined during this session");
    
    [FBAppEvents logEvent:@"FaceBook connect login"];
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            UUID = [self getUUID];
            NSLog(@"uuid %@", UUID);
    
            NSString *paramDefenition = [self encodeParam:UUID];
            NSString *params = [NSString stringWithFormat:@"device_id=%@", paramDefenition];
    
            NSLog(@"fbid %@", fbIDnum);
            NSString *fbIdString = [self encodeParam:fbIDnum];
            NSString *fbParam = [NSString stringWithFormat:@"fb_id=%@", fbIdString];
    
            NSString *prametrs = [NSString stringWithFormat:@"%@&%@",params, fbParam];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@login", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [prametrs dataUsingEncoding:NSUTF8StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&responce
                                                             error:&error];
            
            NSString *resp = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Resoponce for login : %@", resp);
            
            
            //Parse the responce
            if ((![data isEqualToData:nil]) && (!error)) {
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {
                    NSString *status = [responceJSON objectForKey:@"status"];
    
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : login status is %@", status);
                        NSLog(@"Login parameters recieved :");
                        //Update 1.2 
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"allEraLevelsPassed"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        NSString *trId = [responceJSON objectForKey:@"transaction_id"];
                        transactionId = [trId integerValue];
                        clear_uid = [[responceJSON objectForKey:@"clear_uid"] integerValue];
                        clear_fb_id = [[responceJSON objectForKey:@"clear_fb_id"] integerValue];
                        is_main = [[responceJSON objectForKey:@"is_main"] integerValue];
                        NSLog(@"\ntransaction id : %ld \n clear_uid : %d \n clear_fb_id : %d \n is_main : %d", transactionId, clear_uid, clear_fb_id, is_main);
                        logined = 1;
                        loginedNow = 1;
                        BDManager *database = [BDManager sharedBDManager];
                        database.context = FB;
                        [self determineSyncTypeWithFb:fbIDnum];

                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : login failed with status : %@ and the reason is : %@", status, reason);
                    }
                }
                else{   NSLog(@"WARNING : login NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : login NIL data, error : %@", error);  }
        }
    });
    }
}

- (void) logoutRequest
{
    NSLog(@"%s", __FUNCTION__);
    logined = false;
    SLQTSORAppDelegate *del = [[UIApplication sharedApplication] delegate];
    AppViewController *cv = del.rootViewController;
    [cv fbDisconnect];
//    [self reloadIfNeeded];
//    BDManager *database = [BDManager sharedBDManager];
//    database.context = 0;
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"Logout"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            NSString *paramDefenition = [self encodeParam:[self getUUID]];
            NSString *params = [NSString stringWithFormat:@"device_id=%@", paramDefenition];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@logout", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil) {
                    NSString *status = [responceJSON objectForKey:@"status"];
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : logout status is : %@", status);
                        NSString *trId = [responceJSON objectForKey:@"transaction_id"];
                        transactionId = [trId integerValue];
                        NSLog(@"transaction id : %ld", transactionId);
                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : logout failed with status : %@ and the reason is : %@", status, reason);
                    }
                }
                else{   NSLog(@"WARNING : logout NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : logout NIL data, error : %@", error);  }
        }
    });
    loginedNow = 0;
    if ([FBSession activeSession].isOpen) {
        [[FBSession activeSession] close];
        [[FBSession activeSession] closeAndClearTokenInformation];
        [FBSession setActiveSession:nil];
    }
}

//Params : transaction_id , data package, log
- (void) firstSyncRequest
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            ++self.transactionId;
            NSString *trID = [NSString stringWithFormat:@"%ld", transactionId];
            trID = [self encodeParam:trID];
            NSString *params = [NSString stringWithFormat:@"transaction_id=%@", trID];
    
            NSString *dataPackage = [self createDataPackage];
            dataPackage = [self encodeParamJSON:dataPackage];
            NSString *dataParam = [NSString stringWithFormat:@"data=%@", dataPackage];
    
            NSString *log = [self encodeParam:@"real nigga"];
            NSString *logParam = [NSString stringWithFormat:@"log=%@", log];
    
            NSString *prametrs = [NSString stringWithFormat:@"%@&%@&%@",params, dataParam, logParam];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@first_sync", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [prametrs dataUsingEncoding:NSUTF8StringEncoding];
//            request.HTTPBody = [prametrs dataUsingEncoding:NSUTF16StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {

                    //Determinet success of the request
                    NSString *status = [responceJSON objectForKey:@"status"];
    
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : first_sync status is %@", status);
                        NSString *responceData = [responceJSON objectForKey:@"data"];
                        NSLog(@"first_synd data parse");
                        [self holdSyncResult:responceData];
                    }
                    else
                    {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : first_sync failed with status : %@ and reason is : %@", status, reason);
                        SLQTSORAppDelegate *del = [[UIApplication sharedApplication] delegate];
                        AppViewController *cv = del.rootViewController;
                        [cv fbConnectionFailed];
                
                    }
                }
                else{   NSLog(@"WARNING : first_sync NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : first_sync NIL data, error : %@", error);  }
        }
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Syncing"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

//Params : transaction_id , data package, log
- (void) syncRequest
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            ++self.transactionId;
            NSString *trID = [NSString stringWithFormat:@"%ld", transactionId];
            trID = [self encodeParam:trID];
            NSString *params = [NSString stringWithFormat:@"transaction_id=%@", trID];
            
            NSString *dataPackage = [self createDataPackage];
            dataPackage = [self encodeParamJSON:dataPackage];
            NSString *dataParam = [NSString stringWithFormat:@"data=%@", dataPackage];
            
            NSString *log = [self encodeParam:@"real nigga"];
            NSString *logParam = [NSString stringWithFormat:@"log=%@", log];
            
            NSString *prametrs = [NSString stringWithFormat:@"%@&%@&%@",params, dataParam, logParam];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@sync", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [prametrs dataUsingEncoding:NSUTF8StringEncoding];
//            request.HTTPBody = [prametrs dataUsingEncoding:NSUTF16StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            
//            NSString *responceStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSLog(@"SYNC_RESP : %@", responceStr);
            
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {
                    //Determinet success of the request
                    NSString *status = [responceJSON objectForKey:@"status"];
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : sync status is %@", status);
                        NSString *responceData = [responceJSON objectForKey:@"data"];
                        NSLog(@"sync unpack data");
                        [self holdSyncResult:responceData];
                    }
                    else
                    {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : sync failed with status : %@ and reason is : %@", status, reason);
                
                        SLQTSORAppDelegate *del = [[UIApplication sharedApplication] delegate];
                        AppViewController *cv = del.rootViewController;
                        [cv fbConnectionFailed];

                    }
                }
                else{   NSLog(@"WARNING : sync NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : sync NIL data, error : %@", error);  }
        }
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Syncing"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

//Params : transaction_id , data package, log
- (void) sendFbDataRequest
{
//    if (logined && ([[NSUserDefaults standardUserDefaults] integerForKey:@"udUpdated"] > 0)) {
        NSLog(@"%s", __FUNCTION__);
        dispatch_async(fbConnectRequests, ^{
            if ([self checkConnection]) {
                //Configuring request
                ++self.transactionId;
                NSString *trID = [NSString stringWithFormat:@"%ld", transactionId];
                trID = [self encodeParam:trID];
                NSString *params = [NSString stringWithFormat:@"transaction_id=%@", trID];
            
                NSString *dataPackage = [self createDataPackage];
                dataPackage = [self encodeParamJSON:dataPackage];
                NSString *dataParam = [NSString stringWithFormat:@"data=%@", dataPackage];
            
                NSString *log = [self encodeParam:@"real nigga"];
                NSString *logParam = [NSString stringWithFormat:@"log=%@", log];
            
                NSString *prametrs = [NSString stringWithFormat:@"%@&%@&%@",params, dataParam, logParam];
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@send_fb_data", serverAddress]];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setTimeoutInterval:60];
                request.HTTPMethod = @"POST";
                request.HTTPBody = [prametrs dataUsingEncoding:NSUTF8StringEncoding];

                NSError *error;
                NSURLResponse *responce;
                //Send request
                NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            
                if ((![data isEqualToData:nil]) && (!error)) {
                    //Parse the responce
                    NSError *jerror;
                    NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                    if (responceJSON != nil && !jerror) {
                        //Determine success of the request
                        NSString *status = [responceJSON objectForKey:@"status"];
            
                        if ([status isEqualToString:@"ok"]) {
                            NSLog(@"NOTIFICATION : send_fb_data status is %@", status);
                            BDManager *database = [BDManager sharedBDManager];
                            [database dValuesRemove];
                        }
                        else
                        {
                            NSString *reason = [responceJSON objectForKey:@"reason"];
                            NSLog(@"WARNING : send_fb_data failed with status : %@ and reason is : %@", status, reason);
                        }
                    }
                    else{   NSLog(@"WARNING : send_fb_data NIL JSON, error : %@", jerror);  }
                }
                else{   NSLog(@"WARNING : send_fb_data NIL data, error : %@", error);  }
            }
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Syncing"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        });
//    }
//    else
//    {
//        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Syncing"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
}

- (void) getFbContextRequest
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        if ([self checkConnection]) {
            //Configuring request
            NSString *params = @"set_main=1";
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_fb_context", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {
                    //Determinet success of the request
                    NSString *status = [responceJSON objectForKey:@"status"];
            
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : get_fb_context status is %@", status);
                        NSString *responceData = [responceJSON objectForKey:@"data"];
                        NSLog(@"get_fb_context unpack data");
                        [self holdSyncResult:responceData];
                        
//                        //Fix for amulet of vitality purchased
                        BDManager *database = [BDManager sharedBDManager];
                        int maxLifes = [database getIntValue:@"MaxLifes"];
                        int lifes = [database getIntValue:@"userLifes"];
                        NSLog(@"Mx lifes = %d, ul = %d", maxLifes, lifes);
                        
                        if (lifes < maxLifes) {
                            lifes = maxLifes;
                            [database setIntValue:@"userLifes" value:lifes];
                        }
                        
                        
                    }
                    else
                    {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : get_fb_context failed with status : %@ and reason is : %@", status, reason);
                
                        SLQTSORAppDelegate *del = [[UIApplication sharedApplication] delegate];
                        AppViewController *cv = del.rootViewController;
                        [cv fbConnectionFailed];
                    }
                }
                else{   NSLog(@"WARNING : get_fb_context NIL JSON, error : %@", jerror);  }
            }
            else{   NSLog(@"WARNING : get_fb_context NIL data, error : %@", error);  }
        }
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Syncing"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}


#pragma mark Encode/Decode section

- (NSString *) xorEncrypt: (NSString *)string
{
    //Preparation
    NSData *inputString = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *key = [salt dataUsingEncoding:NSUTF8StringEncoding];
    
    //Get bytes
    unsigned char* inputBytes = (unsigned char*)[inputString bytes];
    unsigned char* keyBytes = (unsigned char*)[key bytes];
    
    unsigned int inputLength = [inputString length];
    unsigned int keyLength = [key length];
    unsigned int keyIdx = 0;
    
    //Encode
    for (unsigned int i = 0; i < inputLength; i++) {
        inputBytes[i] = inputBytes[i] ^ keyBytes[keyIdx];
        if (++keyIdx == keyLength) {    keyIdx = 0;   }
    }
    NSData *srData = [NSData dataWithBytes:inputBytes length:[inputString length]];
    //not working
//    return [NSString stringWithUTF8String:[srData bytes]];
    return [[NSString alloc] initWithData:srData encoding:NSUTF8StringEncoding];
}


- (NSString *) encodeParam:(NSString *)string
{
    NSString *param = [NSString stringWithFormat:@"%@%@", string, salt];
//    NSString *param = string;
//    NSLog(@"param + salt %@", param);
//    param = [self xorEncrypt:param];
    NSData *paramData = [param dataUsingEncoding:NSUTF8StringEncoding];
//    NSData *paramData = [param dataUsingEncoding:NSUTF16StringEncoding];

    if ([paramData isEqualToData:nil]) {
        NSLog(@"%s data is nil", __FUNCTION__);
        return nil;
    }
    else
    {
        NSString *result = [NSString base64StringFromData:paramData length:paramData.length];
        return result;
    }
}


- (NSString *) encodeParamJSON:(NSString *)string
{
//    NSString *param = [self xorEncrypt:string];
    NSData *paramData = [string dataUsingEncoding:NSUTF8StringEncoding];
//    NSData *paramData = [string dataUsingEncoding:NSUTF16StringEncoding];

    NSString *result = [NSString base64StringFromData:paramData length:paramData.length];
    return result;
}

- (NSString *) decodeParam:(NSString *)string
{
    NSString *param = string;
    NSData *encodedParam = [NSData base64DataFromString:string];
    if (![encodedParam isEqualToData:nil]) {
        param = [[NSString alloc] initWithData:encodedParam encoding:NSUTF8StringEncoding];
//    NSString *cleanParam = [self xorEncrypt:param];
        NSString *cleanParam = param;
        cleanParam = [cleanParam substringToIndex:(cleanParam.length - salt.length)];
        return cleanParam;
    }
    else{
        NSLog(@"decodeParam bad data");
        return nil;
    }
}


- (NSString *) decodeParamJSON:(NSString *)string
{
    NSString *param = string;
    NSData *encodedParam = [NSData base64DataFromString:string];
    if (![encodedParam isEqualToData:nil]) {
        param = [[NSString alloc] initWithData:encodedParam encoding:NSUTF8StringEncoding];
//    NSString *cleanParam = [self xorEncrypt:param];
        return param;
    }
    else{
        NSLog(@"decodeParamJSON bad data");
        return nil;
    }
}


#pragma mark Additional fb functional


- (void) getFbPicture:(int)levelId
{
    NSLog(@"leveld for king :%d",levelId);
    //Remove the flag
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"LevelKingPicture"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //Remove the old picture if exists
    NSURL *documentDir = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
    NSURL *tmpDir = [[documentDir URLByDeletingLastPathComponent] URLByAppendingPathComponent:@"tmp" isDirectory:YES];
    NSString *tmpDerectory = [tmpDir path];
    
//    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *picName = @"/levelking.png";
    NSString* picFile = [tmpDerectory stringByAppendingPathComponent:picName];
    NSLog(@"Path to check file %@", picFile);
    [[NSFileManager defaultManager] removeItemAtPath:picFile error:nil];
    
    //Send request to clonefish server to get userId
    NSLog(@"%s", __FUNCTION__);
    dispatch_async(fbConnectRequests, ^{
        NSString *kingId;
        if ([self checkConnection]) {
            //Configuring request
            NSString *paramDefenition = [NSString stringWithFormat:@"%d", levelId];

            NSString *params = [NSString stringWithFormat:@"lvlId=%@", paramDefenition];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/device/getBestPlayer", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
//            request.HTTPBody = [params dataUsingEncoding:NSUTF16StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            if ((![data isEqualToData:nil]) && (!error)) {
                //Parse the responce
                NSError *jerror;
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jerror];
                if (responceJSON != nil && !jerror) {
                    NSString *status = [responceJSON objectForKey:@"status"];
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : getBestPlayer status is : %@", status);
                        NSDictionary *data = [responceJSON objectForKey:@"data"];
                        NSLog(@"%@", data);
                        data = [data objectForKey:@"theLvlKing"];
                        kingId = [data objectForKey:@"uid"];
                        NSLog(@"king id : %@", kingId);
            
                        //Download the profile picture from FaceBook
                        NSString *urlString;
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                            urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal&height=140&width=140", kingId];
                        }
                        else
                        {
                            urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal&height=70&width=70", kingId];
                        }

                        NSURL *documentDir = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
                        NSURL *tmpDir = [[documentDir URLByDeletingLastPathComponent] URLByAppendingPathComponent:@"tmp" isDirectory:YES];
                        NSString *tmpDerectory = [tmpDir path];
                        
                        NSString *picName = @"/levelking.png";
                        NSString* picFile = [tmpDerectory stringByAppendingPathComponent:picName];
                        NSLog(@"Path to check file %@", picFile);
                        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:picFile];
                        if (!fileExists) {
                            NSData *musicData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                            [[NSFileManager defaultManager] createFileAtPath:[NSString stringWithFormat:@"%@/%@", tmpDerectory, picName] contents:musicData attributes:nil];
                            tmpDerectory = [tmpDerectory stringByAppendingString:@"/levelking.png"];
                            
                            if ([[NSFileManager defaultManager] fileExistsAtPath:tmpDerectory]) {
                                NSLog(@"Picture of the level king downloaded %@", tmpDerectory);
                                if ([self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:tmpDerectory]]) {
                                    NSLog(@"Skip tag added");
                                }
                                else
                                {
                                    NSLog(@"Skip tag not added");
                                }
                                
                                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"LevelKingPicture"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            else{
                                NSLog(@"Picture of the level king not downloaded %@", tmpDerectory);
                            }
                        }
                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : getBestPlayer failed with status : %@ and the reason is : %@", status, reason);
                    }
                }
                else {  NSLog(@"WARNING: get_fb_picture NIL JSON, error : %@", jerror); };
            }
            else {   NSLog(@"WARNING: get_fb_picture NIL data, error : %@", error);   }
        }
    });
}


- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


- (NSDictionary *) getLevelTop: (int)levelId
{
    NSMutableDictionary *retDict = [[NSMutableDictionary alloc] init];
    NSLog(@"%s", __FUNCTION__);
        if ([self checkConnection]) {
            //Configuring request
            NSString *paramDefenition = [NSString stringWithFormat:@"%d", levelId];
            
            NSString *params = [NSString stringWithFormat:@"lvlId=%@", paramDefenition];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/device/getLevelTop", serverAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setTimeoutInterval:60];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
//            request.HTTPBody = [params dataUsingEncoding:NSUTF16StringEncoding];

            NSError *error;
            NSURLResponse *responce;
            //Send request
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&responce
                                                             error:&error];
            if (![data isEqualToData:nil]) {
                //Parse the responce
                NSDictionary *responceJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                if (responceJSON != nil) {
                    NSLog(@"JSON : %@", responceJSON);
            
                    NSString *status = [responceJSON objectForKey:@"status"];
                    NSLog(@"status : %@", status);
            
                    if ([status isEqualToString:@"ok"]) {
                        NSLog(@"NOTIFICATION : getBestPlayer status is : %@", status);
                        NSDictionary *data = [responceJSON objectForKey:@"data"];
                        NSArray *bestPlayers = [data objectForKey:@"theBestPlayers"];
                        NSMutableArray *scores = [[NSMutableArray alloc] init];
                        NSMutableArray *uids = [[NSMutableArray alloc] init];
                        for (int i = 0; i < 15; i++) {
                            NSDictionary *player = [bestPlayers objectAtIndex:i];
                            [scores addObject:[player objectForKey:@"score"]];
                            [uids addObject:[player objectForKey:@"uid"]];
                        }
                        [retDict setObject:scores forKey:@"scores"];
                        [retDict setObject:uids forKey:@"uids"];
                    }
                    if ([status isEqualToString:@"fail"]) {
                        NSString *reason = [responceJSON objectForKey:@"reason"];
                        NSLog(@"WARNING : getBestPlayer failed with status : %@ and the reason is : %@", status, reason);
                    }
                }
            }
        }
    return retDict;
}


@end



