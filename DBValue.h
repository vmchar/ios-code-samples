//
//  DBValue.h
//
//
//  Created by Vlad on 20.03.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBValue : NSObject

- (void) localSet: (NSString *)key value:(int) val base: (sqlite3 *) db;

- (void) fbSet: (NSString *)key value:(int) val base: (sqlite3 *) db;

- (int) get: (NSString *)query base: (sqlite3 *) db;

- (void) dropFb: (sqlite3 *)db;

@end
