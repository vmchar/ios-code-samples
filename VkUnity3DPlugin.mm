//
//  VkDelegate.m
//  Vk native implementation for Unity plugin.
//
//  Created by Vlad Chartanovich on 18.09.15.
//  vmchar@outlook.com
//  Copyright (c) 2015 Vlad Chartanovich. All rights reserved.
//
//  For Unity version 4.30 or higher.
//
//  Avaliable permissions for VkSdk:
//  @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_AUDIO, VK_PER_PHOTOS, VK_PER_NOHTTPS, VK_PER_EMAIL, VK_PER_MESSAGES];
//  Using VK_PER_MESSAGES may cause error after login on iOS. You need to request permission from vk.com for using it in your app.
//

//#define RELEASE
//
//#ifdef RELEASE
//#define NSLog(...) ;
//#endif

#import "VkDelegate.h"

//Singleton object
static VkDelegate *_instance = [VkDelegate sharedInstance];
//Name of Unity gameobject to send message
const char *gameObjectName = "VKUnityBridge";


@implementation VkDelegate

#pragma mark - Public Methods

+ (void) initialize
{
    if(!_instance)
    {
        _instance = [[VkDelegate alloc] init];
    }
}

//Custom initializer. Creates instance and initializes VkSDK
+ (VkDelegate*)initSDKWithAppID: (NSString*) appId
{
    VkDelegate *instance = [[VkDelegate alloc] initWithAppId:appId];
    instance->SCOPE = [instance setScope];
    [instance startSDK];
    return instance;
}

+ (VkDelegate*) sharedInstance
{
    return _instance;
}

#pragma mark - Private Methods

//Constructor
- (id) initWithAppId: (NSString *) appId
{
    self = [self init];
    if(self)
    {
        [self setAppId:appId];
        return self;
    }
    return nil;
}

//Cache application id
- (void) setAppId: (NSString *) appId
{
    _instance->applicationId = appId;
}

- (id) init
{
    if(_instance != nil)
        return _instance;
    
    self = [super init];
    
    if(!self)
        return nil;
    
    _instance = self;

    //Get app delegate methods
    UnityRegisterAppDelegateListener(self);
    
    return _instance;
}

//Vk SDK first call
- (void)startSDK
{
    [VKSdk initializeWithDelegate:self andAppId:self->applicationId];
}

//Set the permissions array
- (NSArray*) setScope
{
    return @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_AUDIO, VK_PER_PHOTOS, VK_PER_NOHTTPS];
}

//Try to login
- (void)login
{
    if([VKSdk wakeUpSession]){
        NSLog(@"Vk session awaken");
        //Call success login manually
        [self onLoginFinished:[VKSdk getAccessToken]];
    }
    //No living sessions. Need to login
    else
    {
        NSLog(@"Vk session wake up failure");
        [VKSdk authorize:SCOPE];
    }
}

//Logout
- (void)logout
{
    [VKSdk forceLogout];
}

//User login status
- (BOOL)isLogined
{
    return [VKSdk isLoggedIn];
}

//Get a pair of token + ";" + userId
- (NSString*) getAccessToken
{
    VKAccessToken *token = [VKSdk getAccessToken];
    return [NSString stringWithFormat:@"%@;%@",token.accessToken, token.userId ];
}

//Call for VK API method
- (void) callAPIWithMethod: (NSString *)method parameters: (NSDictionary *)params
{
    //Create request
    VKRequest *apiRequest = [VKRequest requestWithMethod:method andParameters:params andHttpMethod:@"POST"];
    
    //Execute with callbacks
    [apiRequest executeWithResultBlock:
        ^(VKResponse *response)
        {
            //Convert response string
            const char *message = nil;
            NSString *responceString = response.responseString;
            message = [responceString cStringUsingEncoding:NSUTF8StringEncoding];
            //Send message to Unity
            UnitySendMessage(gameObjectName, "onRequestFinished", message == nil ? "" : message);
        }
        errorBlock:^(NSError *error)
        {
            [[VkDelegate sharedInstance] onApiError:error];
        }
     ];
}

#pragma mark - Events

//Login finished callback
- (void) onLoginFinished: (VKAccessToken*) accesstoken
{
    NSLog(@"VK Login finished");
    //Convert token
    const char *message = nil;
    NSString *response = [NSString stringWithFormat:@"%@;%@", accesstoken.accessToken, accesstoken.userId];
    message = [response cStringUsingEncoding:NSUTF8StringEncoding];
    //Call unity method with response
    UnitySendMessage(gameObjectName, "onLoginFinished", message == nil ? "" : message);

}

//Login failed callback
- (void) onLoginFailed
{
    NSLog(@"VK login failed");
    //Call unity to tell about fail
    UnitySendMessage(gameObjectName, "onLoginFinished", ";");
}

//Request failed callback
- (void) onApiError: (NSError *) error
{
    NSLog(@"VK recieved Api error");
    //Convert message
    const char *message = nil;
    NSString *errorMessage = error.description;
    message = [errorMessage cStringUsingEncoding:NSUTF8StringEncoding];
    //Call unity method
    UnitySendMessage(gameObjectName, "onRequestFailed", message == nil ? ";" : message);
}

#pragma mark - App Delegate Methods

//Handle url open
- (BOOL)openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication {
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    return YES;
}

#pragma mark - VkSDKDelegate Methods

#pragma mark Requered

//Called after successful login
-(void) vkSdkReceivedNewToken:(VKAccessToken*) newToken
{
    NSLog(@"VK auth success with token: %@", newToken.accessToken);
    [self onLoginFinished:newToken];
}

/**
 Calls when user must perform captcha-check
 @param captchaError error returned from API. You can load captcha image from <b>captchaImg</b> property.
 After user answered current captcha, call answerCaptcha: method with user entered answer.
 */
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    NSLog(@"VK needs to show captcha. Description: %@", captchaError.description);
    //Create capthca view controller
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    //Show captcha
    UIViewController *mainVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    [mainVC presentViewController:vc animated:YES completion:nil];
}

/**
 Notifies delegate about existing token has expired
 @param expiredToken old token that has expired
 */
- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    NSLog(@"VK token expired, token: %@", expiredToken.accessToken);
    //Relogin user
    [self login];
}

/**
 Notifies delegate about user authorization cancelation
 @param authorizationError error that describes authorization error
 */
- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    NSLog(@"VK auth error %@", authorizationError.description);
    [self onLoginFailed];
}

/**
 Pass view controller that should be presented to user. Usually, it's an authorization window
 @param controller view controller that must be shown to user
 
 ** Login dialog is passed here
 */
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    NSLog(@"VK should present view controller");
    
    UIViewController *mainVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    [mainVC presentViewController:controller animated:YES completion:nil];
}

/**
 Notifies delegate about receiving new access token
 @param newToken new token for API requests
 */
-(void) vkSdkDidReceiveNewToken:(VKAccessToken*) newToken
{
    NSLog(@"VK recieved new token: %@", newToken.accessToken);
}

#pragma mark Optional

/**
 Notifies delegate about receiving predefined token (initializeWithDelegate:andAppId:andCustomToken: token is not nil)
 @param token used token for API requests
 */
- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token
{
    NSLog(@"VK accepted token: %@", token.accessToken);
}

/**
 Notifies delegate about receiving new access token
 @param newToken new token for API requests
 */
- (void)vkSdkRenewedToken:(VKAccessToken *)newToken
{
    NSLog(@"VK renewed token: %@", newToken.accessToken);
}

/**
 Requests delegate about redirect to Safari during authorization procedure.
 By default returns YES
 */
- (BOOL)vkSdkAuthorizationAllowFallbackToSafari
{
    return YES;
}

/**
 Applications which not using VK SDK as main way of authentication should override this method and return NO.
 By default returns YES.
 */
- (BOOL)vkSdkIsBasicAuthorization
{
    return YES;
}

/**
 * Called when a controller presented by SDK will be dismissed
 */
- (void)vkSdkWillDismissViewController:(UIViewController *)controller
{
    NSLog(@"VK will dismiss view controller");
}

/**
 * Called when a controller presented by SDK did dismiss
 */
- (void)vkSdkDidDismissViewController:(UIViewController *)controller
{
    NSLog(@"VK did dismiss view controller");
}

@end

#pragma mark - Helper Functions

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

#pragma mark - Unity callable

//This methods are visible for Unity
// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
    
    void _initVk(const char *_appId)
    {
        [VkDelegate initSDKWithAppID:[NSString stringWithUTF8String:_appId]];
    }
    
    void _loginVk()
    {
        [[VkDelegate sharedInstance] login];
    }
    
    void _logoutVk()
    {
        [[VkDelegate sharedInstance] logout];
    }
    
    bool _isLoginedVk()
    {
        return [[VkDelegate sharedInstance] isLogined];
    }
    
    const char* _getAccessTokenVk()
    {
        return MakeStringCopy([[[VkDelegate sharedInstance] getAccessToken] UTF8String]);
    }
    
    void _callAPIVk(const char* method, const char* parameters)
    {
        // (const char *)json -> nsstring -> nsdata -> nsdictionary
        NSString *convMethod = CreateNSString(method);
        
        NSString *stringParams = CreateNSString(parameters);
        NSData *paramData = [stringParams dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *paramDictionary = [NSJSONSerialization JSONObjectWithData:paramData options:0 error:nil];
        
        //TODO: Debug dictionary (!!!)
        [[VkDelegate sharedInstance] callAPIWithMethod:convMethod parameters:paramDictionary];
    }
}


