//
//  DBValue.m
//
//  Created by Vlad on 20.03.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//
//
//
// @comment : this class is a convinient use of database within the application.
//            works only with int values. May be extended for usage other types
//
//
// @interface:
//
//      1) localSet:(NSString *)key value:(int)val base:(sqlite3 *)db
//                  sets value val in database table Local Progress for the key key
//
//      2) fbSet:(NSString *)key value:(int)val base:(sqlite3 *)db
//                  sets value val in database table FaceBook Progress for the key key
//
//      3) get: (NSString *)query base: (sqlite3 *) db
//                  works with already made queries. Returns value for the keys
//
//      4) dropFb: (sqlite3 *)db
//                  implemented only for FB connect. User may change fb accaunt while
//                  playing. So there is need to remove all values from current fb table.
//                  Doesn't actually 'drop table' sqlite command, just removes all values
//                  for every key in FaceBook Progress table
//
//
//

#import "DBValue.h"
#import <sqlite3.h>
#import "DBGlobal.h"


@implementation DBValue


- (id)init
{
    if((self = [super init]))
    {    }
    return self;
}


- (void) localSet:(NSString *)key value:(int)val base:(sqlite3 *)db
{
    NSString *query = @"insert into LocalProgress values (?, ?)";
    dispatch_sync(dbQueue, ^{
        sqlite3_stmt *add = Nil;
        if (sqlite3_prepare_v2(db, [query UTF8String], -1, &add, NULL) != SQLITE_OK) {
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(db));
        }
        sqlite3_bind_text(add, 1, [key UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(add, 2, val);
        if (sqlite3_step(add) != SQLITE_DONE) {
            NSLog(0, @"Error while inserting data message =  '%s'", sqlite3_errmsg(db));
        }
        else {
            sqlite3_reset(add);
        }
    });
}


- (void) fbSet:(NSString *)key value:(int)val base:(sqlite3 *)db
{
    NSString *query = @"insert into FBProgress values (?, ?)";
    dispatch_sync(dbQueue, ^{
        sqlite3_stmt *add = Nil;
        if (sqlite3_prepare_v2(db, [query UTF8String], -1, &add, NULL) != SQLITE_OK) {
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(db));
        }
        sqlite3_bind_text(add, 1, [key UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(add, 2, val);
        if (sqlite3_step(add) != SQLITE_DONE) {
            NSLog(0, @"Error while inserting data message =  '%s'", sqlite3_errmsg(db));
        }
        else {
            sqlite3_reset(add);
        }
    });
}


- (int) get: (NSString *)query base: (sqlite3 *) db
{
    __block int retVal = 0;
    dispatch_sync(dbQueue, ^{
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil)
            == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {   retVal = sqlite3_column_int(statement, 0);  }
        }
        else{   NSLog(@"%s Error while fetching value", __FUNCTION__);  }
    });
    return retVal;
}


- (void) dropFb: (sqlite3 *)db
{
    dispatch_sync(dbQueue, ^{
        NSString *drop = @"delete from FBProgress";
        char *errMsg;
        if (sqlite3_exec(db, [drop UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Error while dropping the base %s", errMsg);
        }
        else{   NSLog(@"FBProgress successfully dropped");  }
    });
}

@end
