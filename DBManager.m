//
//  BDManager.m
//
//  Created by Vlad on 22.01.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//
//  DataBase manager used to store user progress.
//  DataBase file: data.sqlite3
//  DataBase location: app's Documents directory
//
//  Tables:
//          LocalProgress (text key, integer value)
//          FBProgress (text key, integer value)
//
//          ---- Class Description ----
//
//  1)Parameters: (most like in SettingsManager)
//          UserLifes  --  amount of lifes
//          UserCash  --  amount of Game coins
//          UserDiamonds  --  amount of Diamonds
//
//
//  2)Properties:
//      -_database - instance of the database
//      -context - from enum context {local, FB} in Global.h
//
//
//  3)Methods:
//      -init -determine local or FB context, get path to DB
//      -firstLaunch - check if tables exists and create them if not
//      -setFirstTimeProgress - set needed params and prepare context to play if it's app's first launch
//      -(int) getIntValue:(NSString *)key - determines the context and returns value for the key
//      -changeIntValue:(NSString *)key value:(int *)val - gets the existing value from DB,
//                                                          changes it by val and saves back to the table,
//                                                          context is determined automatically
//      -setIntValue:(NSString *)key value:(int *)val - rewrites existing or sets new value at the table,
//                                                      context is determined automatically
//      -restoreLifes - checks the time passed from the last app close. Counts how much lifes restored
//                      and timeElapsed since that moment

#import "BDManager.h"
#import <sqlite3.h>
#import "Global.h"
#import "SingletonArc.h"
#import "DBValue.h"
#import "UDLifeRestore.h"
#import "RReachability.h"
#import "FbConnectManager.h"
dispatch_queue_t dbQueue;

@implementation BDManager

@synthesize  context = _context;
@synthesize _database;

- (void) dealloc
{   sqlite3_close(_database);   }

int getRetVal;

#pragma mark Initialize section

SYNTHESIZE_SINGLETON_FOR_ARC_CLASS(BDManager);

- (void) setContext:(uint)newContext{
    NSLog(@"Switching context %d", newContext);
    NSString *cnt;
    (newContext == 0) ? (cnt = @"Local") : (cnt = @"Facebook");
    NSLog(@"Try to change context to %@", cnt);
    _context = newContext;
    NSLog(@"_context : %d", _context);
    int inited = [self getIntValue:@"inited"];
    if (inited < 1) {
        [self setIntValue:@"inited" value:1];
        [self setIntValue:@"userLifes" value:3];
    }
    [self calculateMaxLifes];
    NSString *k;
    (_context == local) ? (k = @"Local"): (k = @"Facebook");
    NSLog(@"Got context %@", k);
}

- (uint) context{  return [NSNumber numberWithUnsignedInt:_context];   }

- (BOOL) checkConnection
{
    RReachability *reach = [RReachability reachabilityWithHostName:@"bubblespredev-ios.clonefish.com"];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus != NotReachable) {    return YES;   }
    else{
        NSLog(@"WARNING: No internet connection");
        return NO;
    }
}


- (id) init
{
    if((self = [super init]))
    {
        
        dbQueue = dispatch_queue_create("com.clonefish.bubblechronicles.dbQueue", DISPATCH_QUEUE_SERIAL);

        FbConnectManager *fb = [[FbConnectManager alloc] init];
        [fb initRequest];
        [fb pingRequest];
        
        context = local;
        
        //Path to BD
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDir = paths[0];
        NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"data.sqlite3"];
        if (sqlite3_open([dbPath UTF8String], &_database) == SQLITE_OK)
        {
            NSLog(@"Database successfully opened!");
            if ([self firstLaunch]) {
                NSLog(@"Table LocalProgress exists or created");
            }
            else{ NSLog(@"Error while creating table"); }
        }
        else{   NSLog(@"Failed to open/create database!");  }
    }
    [self calculateMaxLifes];
    return self;
}

//Check for LocalProgress table
- (bool) firstLaunch
{
    NSLog(@"%s", __FUNCTION__);
    char *errMsg;
    NSString *sql_stmt = @"create table if not exists LocalProgress (key text, value integer, unique(key) on conflict replace );";
    if (sqlite3_exec(_database, [sql_stmt UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
        NSLog(@"Failed to create table at bd with message = %s", errMsg);
        return  false;
    }
    else {
        //Check if first launch progress recorded
        NSString *query = @"select value from LocalProgress where key = 'ingameCash'";
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
            == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                //parce fields
                int uniqueId = sqlite3_column_int(statement, 0);
                NSLog(@"Recieved data from DataBase = %d", uniqueId);
            }
            else{
                NSLog(@"DataBase is empty");
                [self setFirstTimeProgress];
            }
        }
        else{   NSLog(@"Failed to SELECT values from table");   }
        if ([self createFBTable]) { return  true; }
        else{
            NSLog(@"Failed to create or check FBProgress table");
            return false;
        }
    }
}

- (BOOL) createFBTable
{
    NSLog(@"%s", __FUNCTION__);
    char *errMsg;
    NSString *sql_stmt = @"create table if not exists FBProgress (key text, value integer, unique(key) on conflict replace);";
    if (sqlite3_exec(_database, [sql_stmt UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
        NSLog(@"Failed to create table at bd with message = %s", errMsg);
        return  false;
    }
    else{
        NSLog(@"FBProgress table exists or created");
        return true;
    }
}

- (void) dropFbTable
{
    DBValue *dropValue = [[DBValue alloc] init];
    [dropValue dropFb:_database];
    if ([self createFBTable]) { NSLog(@"Facebook table recreated"); }
    else{   NSLog(@"error while recreating Facebook table");    }
}

- (void) recreateFbContext
{
    NSLog(@"%s", __FUNCTION__);
    dispatch_sync(dbQueue, ^{
        char *err;
        NSString *drop_stmt = @"drop table FBProgress";
        if (sqlite3_exec(_database, [drop_stmt UTF8String], NULL, NULL, &err) != SQLITE_OK) {
            NSLog(@"FBProgress table dropped Failed to drop table FBProgress with message = %s", err);
        }
        else
        {
            NSLog(@"FBProgress table dropped");
            NSString *sql_stmt = @"create table if not exists FBProgress (key text, value integer, unique(key) on conflict replace);";
            if (sqlite3_exec(_database, [sql_stmt UTF8String], NULL, NULL, &err) != SQLITE_OK) {
                NSLog(@"Failed to create table at bd with message = %s", err);
            }
            else{
                NSLog(@"FBProgress table exists or created");
            }
        }
        });
}

- (void) diamondsTest{   [self setIntValue:@"ingameDiamonds" value:50];  }

- (void) setFirstTimeProgress
{
    //Update 1.2 fix
    //Changed first time diamonds balance from 5 to 2

    
    //Most common 3 params
    NSArray *keys = [NSArray arrayWithObjects:@"userLifes", @"ingameCash", @"ingameDiamonds", nil];
//    int values[3] = {3, 4000, 5};
    int values[3] = {3, 4000, 2};
    
    [self setdValue:@"dingameCash" value:4000 oldValue:0];
    
//    [self setdValue:@"dingameDiamonds" value:5 oldValue:0];
    
    [self setdValue:@"dingameDiamonds" value:2 oldValue:0];
    
    for (int i = 0; i < 3; i++) {
        [self setIntValue:keys[i] value:values[i]];
    }
    [self setIntValue:@"lastPassedLevelNumber" value:0];

    
    //User doesn't own amulets at 1st launch
    [self setIntValue:@"Abundance" value:0];
    [self setIntValue:@"Eternal_Life" value:0];
    [self setIntValue:@"Vitality" value:0];
    [self setIntValue:@"Reflexio" value:0];
    [self setIntValue:@"Oracle" value:0];
    [self setIntValue:@"Strategy" value:0];
    [self setIntValue:@"Tesla" value:0];
    [self setIntValue:@"Constancy" value:0];
    [self setIntValue:@"Defrostio" value:0];
    
    [self calculateMaxLifes];
    
    //User don't have boosts
    for (int i = 0; i < 5; i++) {
        [self setIntValue:[NSString stringWithFormat:@"Boost%d", i] value:0];
    }
    
    //User don't have any collection items and keys
    for (int i = 0; i < 7; i++) {
        [self setIntValue:[NSString stringWithFormat:@"%dEraKeys", i] value:0];
        for (int j = 0; j < 6; j++) {
            [self setIntValue:[NSString stringWithFormat:@"%dEra%dItem", i, j] value:0];
        }
    }

    for (int i = 0; i <7; i++) {
        [self setIntValue:[NSString stringWithFormat:@"era%dStars", i] value:0];
    }

#pragma mark Test Progress Mode
//  Uncomment next 3 lines to get
//  advanced progress at 6th era with 60 passed levels
//    [self setIntValue:@"currentEraId" value:6];
//    [self setIntValue:@"lastOpenedEraId" value:6];
//    [self setIntValue:@"lastPassedLevelNumber" value:63];
    
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"noEraPlayed"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Set all dValues to 0 after sync with FB
- (void) dValuesRemove
{
    [self setIntValue:@"dingameCash" value:0];
    [self setIntValue:@"dingameDiamonds" value:0];
    //Drop dKeys and dItems
    for (int i = 0; i < 7; i++) {
        [self setIntValue:[NSString stringWithFormat:@"d%dEraKeys", i] value:0];
        for (int j = 0; j < 6; j++) {
            [self setIntValue:[NSString stringWithFormat:@"d%dEra%dItem", i, j] value:0];
        }
    }
    //Drop dBoosts
    for (int i = 0; i < 5; i++) {
        [self setIntValue:[NSString stringWithFormat:@"dBoost%d", i] value:0];
    }
    //Set flag that no data was updated, so no need to send send_fb_data request
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"udUpdated"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) restoreLifes{  UDLifeRestore *res = [[UDLifeRestore alloc] init];    }

#pragma mark Values setters/getters

- (int) getIntValue:(NSString *)key
{
    NSString *getQuery;
    if (_context == local) {    getQuery = [NSString stringWithFormat:@"select value from LocalProgress where key = '%@'", key];    }
    else{   getQuery = [NSString stringWithFormat:@"select value from FBProgress where key = '%@'", key];   }
    DBValue *value = [[DBValue alloc] init];
     return [value get:getQuery base:_database];
}

- (void) setIntValue:(NSString *)key value:(int)val
{
    DBValue *value = [[DBValue alloc] init];
    if (_context == local)
    {   [value localSet:key value:val base:_database];  }
    else {  [value fbSet:key value:val base:_database]; }
}

-   (void) setdValue:(NSString *)key value:(int)val oldValue:(int)oldValue
{
    int difference = val - oldValue;
    int curDifference = [self getIntValue:key];
    curDifference += difference;
    [self setIntValue:key value:curDifference];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"udUpdated"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


- (void) testCheck
{
    int money = [self getIntValue:@"ingameCash"];
    int diamonds = [self getIntValue:@"ingameDiamonds"];
    int lifes = [self getIntValue:@"userLifes"];
    NSLog(@"TEST DB: money = %d, diamonds = %d, lifes = %d", money, diamonds, lifes);
    sqlite3_close(_database);
    [self setIntValue:@"ingameCash" value:666999];
    [self setIntValue:@"UserDiamonds" value:666];
    money = [self getIntValue:@"ingameCash"];
    diamonds = [self getIntValue:@"UserDiamonds"];
    NSLog(@"TEST DB setter: money = %d diamonds = %d", money, diamonds);
        money = [self getIntValue:@"ingameCash"];
    diamonds = [self getIntValue:@"UserDiamonds"];
    NSLog(@"TEST DB changer: money = %d diamonds = %d", money, diamonds);
}

#pragma mark Key values helpers

- (int) getCurrentEra {   return [self getIntValue:@"currentEraId"];    }

- (void) calculateMaxLifes
{
    if ([self getIntValue:@"Eternal_Life"] > 0) {
        NSLog(@"Etenal Lifes calculated");
        [self setIntValue:@"MaxLifes" value:100];
        [self setIntValue:@"userLifes" value:100];
    }
    else
    {
        if ([self getIntValue:@"Vitality"] > 0) {   [self setIntValue:@"MaxLifes" value:6]; }
        else{   [self setIntValue:@"MaxLifes" value:3]; }
    }
    NSLog(@"Maximum lifes = %d", [self getIntValue:@"MaxLifes"]);
}

@end
