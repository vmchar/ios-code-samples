//
//  FbConnectManager.h
//
//  Created by Vlad on 20.02.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SingletonArc.h"
//#import "SynthesizeSingleton.h"

@interface FbConnectManager : NSObject
{
    NSString *UUID;
    NSMutableArray *requests;
    long int transactionId;
    long int fbId;
    NSString *serverAddress;
    NSString *salt;
    
    int clear_fb_id;
    int clear_uid;
    int is_main;
    int logined;
    int loginedNow;
}
@property (nonatomic, retain) NSString *UUID;
@property (nonatomic, retain) NSString *salt;
@property (nonatomic) long int transactionId;
@property (nonatomic) long int fbId;
@property (nonatomic) int clear_fb_id, clear_uid, is_main, logined;
@property (nonatomic) int loginedNow;

//Helpers
+ (FbConnectManager *) sharedFbConnectManager;
- (NSString *) createUUID;
- (NSString *) getUUID;
- (BOOL) checkConnection;
- (NSString *) createDataPackage;
- (void) unpackData : (NSString *) recievedDataPackage;
- (void) showCookies;
- (void) determineSyncTypeWithFb: (NSString *) fbId;
- (void) holdSyncResult: (NSString *) data;
- (void) reloadIfNeeded;


//FB connect requests
- (void) initRequest;
- (void) pingRequest;
- (void) loginRequest: (NSString *) fbId;
- (void) logoutRequest;
- (void) syncRequest;
- (void) firstSyncRequest;
- (void) getFbContextRequest;
- (void) sendFbDataRequest;

//Encode/decode
- (NSString *) xorEncrypt: (NSString *)string;
- (NSString *) encodeParam: (NSString *)string;
- (NSString *) encodeParamJSON: (NSString *)string;
- (NSString *) decodeParam: (NSString *)string;
- (NSString *) decodeParamJSON:(NSString *)string;

//Additional fb functions
- (void) getFbPicture: (int)levelId;
- (NSDictionary *) getLevelTop: (int)levelId;

@end
