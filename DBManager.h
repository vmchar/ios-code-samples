//
//  BDManager.h
//
//  Created by Vlad on 22.01.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SingletonArc.h"
//#import "SynthesizeSingleton.h"

@interface BDManager : NSObject
{
    sqlite3 *_database;
    uint context;
}

@property (nonatomic) uint context;
@property (nonatomic) sqlite3 *_database;

+ (BDManager *) sharedBDManager;

//Initialize section
- (bool) firstLaunch;
- (BOOL) createFBTable;
- (void) dropFbTable;
- (void) setFirstTimeProgress;
- (void) recreateFbContext;
- (void) dValuesRemove;

//Value setters/getters
- (int) getIntValue: (NSString *) key;
- (void) setIntValue: (NSString *)key  value: (int)val;
- (void) setdValue: (NSString *)key value: (int)val oldValue: (int)oldValue;

- (void) testCheck;
- (void) diamondsTest;

//Key values helpers
- (int) getCurrentEra;
- (void) calculateMaxLifes;

//Launch restore lifes
- (void) restoreLifes;

- (BOOL) checkConnection;


@end
