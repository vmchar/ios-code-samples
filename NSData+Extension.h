//
//  NSData+Extension.h
//
//  Created by Vlad on 21.02.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Extension)

+ (NSData *) base64DataFromString:(NSString *)string;

@end
