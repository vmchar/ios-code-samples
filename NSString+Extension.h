//
//  NSString+Extension.h
//
//  Created by Vlad on 21.02.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

+ (NSString *) base64StringFromData:(NSData *)data length:(int)length;

@end
