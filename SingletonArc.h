//
//  SingletonArc.h
//
//  Created by Vlad on 19.03.14.
//  Copyright (c) 2014 Vladislav Chartanovich. All rights reserved.
//


#define SYNTHESIZE_SINGLETON_FOR_ARC_CLASS(classname) \
\
+ (classname *)shared##classname \
{ \
static classname *shared##classname = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##classname = [[classname alloc] init]; \
}); \
return shared##classname; \
} \

